package ribomation.rest_ws.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ribomation.rest_ws.domain.Product;

import java.io.IOException;
import java.net.URI;
import java.net.http.*;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow;

import static java.net.http.HttpClient.Redirect.*;
import static java.time.Duration.*;

public class ClientApp {
    final String BASE_URI = "http://localhost:9000/api/products";
    final Gson   GSON     = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create();

    public static void main(String[] args) throws Exception {
        ClientApp app = new ClientApp();
        app.run();
    }

    public void run() throws IOException, InterruptedException {
        get(42);
        var id = create("Java 11 Primer");
        update(id, "Modern Java Tutorial");
        remove(id);
        try {
            get(id);
        } catch (IllegalArgumentException e) {
            System.out.printf("successfully removed product id=%d%n", id);
        }
    }

    void get(long id) throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder(URI.create(BASE_URI + "/" + id)).build();
        var res = client().send(req, HttpResponse.BodyHandlers.ofString());
        if (res.statusCode() == 200) {
            Product product = GSON.fromJson(res.body(), Product.class);
            System.out.printf("PRODUCT: %s%n", product);
        } else {
            throw new IllegalArgumentException("fetch failed");
        }
    }

    long create(String name) throws IOException, InterruptedException {
        var product = new Product();
        product.setName(name);
        product.setPrice(100);
        product.setItems(2);
        var req = HttpRequest.newBuilder(URI.create(BASE_URI))
                .POST(HttpRequest.BodyPublishers.ofString(GSON.toJson(product)))
                .build();
        var res = client().send(req, HttpResponse.BodyHandlers.ofString());
        if (res.statusCode() == 201) {
            product = GSON.fromJson(res.body(), Product.class);
            System.out.printf("CREATED: %s%n", product);
            return product.getId();
        } else {
            throw new IllegalArgumentException("insertion failed");
        }
    }

    void update(long id, String name) throws IOException, InterruptedException {
        var product = new Product();
        product.setName(name);
        var req = HttpRequest.newBuilder(URI.create(BASE_URI + "/" + id))
                .PUT(HttpRequest.BodyPublishers.ofString(GSON.toJson(product)))
                .build();
        var res = client().send(req, HttpResponse.BodyHandlers.ofString());
        if (res.statusCode() == 200) {
            product = GSON.fromJson(res.body(), Product.class);
            System.out.printf("UPDATED: %s%n", product);
        } else {
            throw new IllegalArgumentException("update failed");
        }
    }

    void remove(long id) throws IOException, InterruptedException {
        var req = HttpRequest.newBuilder(URI.create(BASE_URI + "/" + id))
                .DELETE()
                .build();

        // these calls will hang, must be a bug !!
        //var res = client().send(req, HttpResponse.BodyHandlers.discarding());
        //var res = client().send(req, HttpResponse.BodyHandlers.replacing(null));

        var res = client().send(req, (HttpResponse.BodyHandler<String>) r -> new HttpResponse.BodySubscriber<>() {
            public CompletionStage<String> getBody() {
                return CompletableFuture.supplyAsync(() -> "");
            }
            public void onSubscribe(Flow.Subscription subscription) {
                subscription.cancel();
            }
            public void onNext(List<ByteBuffer> item) { }
            public void onError(Throwable throwable) { }
            public void onComplete() { }
        });

        if (res.statusCode() != 204) {
            throw new IllegalArgumentException("delete failed");
        }
    }

    HttpClient client() {
        return HttpClient.newBuilder()
                .followRedirects(NORMAL)
                .connectTimeout(ofSeconds(5))
                .build();
    }

}
