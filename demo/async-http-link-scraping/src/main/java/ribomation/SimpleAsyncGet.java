package ribomation;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class SimpleAsyncGet {
    public static void main(String[] args) throws Exception {
        SimpleAsyncGet app = new SimpleAsyncGet();
//        app.run("https://www.ribomation.se/index.html");
        app.run("https://www.ribomation.se/courses/java/java-10.html");
    }

    private void run(String url) {
        var req = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();

        var http = HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.ALWAYS)
                .connectTimeout(Duration.ofSeconds(10))
                .build();

        http.sendAsync(req, HttpResponse.BodyHandlers.ofString())
                .thenApply((HttpResponse res) -> res.body().toString())
                .thenAccept((String txt) -> System.out.printf("TEXT: %s%n", txt))
                .join();
    }

}
