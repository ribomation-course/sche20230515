Async HTTP Link/HTML Scraper Demo
====

This is a demonstration of
* Async HTTP operation using Java 11 HttpClient
* Async computation using  CompletableFuture<T>
* HTML scraping using Jsoup

Prerequisites
----
Ensure you have Java version 11 installed and activated

    $ java --version
    openjdk 11.0.1 2018-10-16

Execution
----

Compile and run using Gradle wrapper

    $ ./gradlew run
    ...list of urls...
    ...list of course data...

Run the unit tests with

    $ ./gradlew test


