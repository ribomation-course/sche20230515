package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordCloudApp {
    public static void main(String[] args) throws Exception {
        var app = new WordCloudApp();
        app.run("./musketeers.txt", 4, 100);
    }

    void run(String filename, int minSize, int maxWords) throws IOException {
        var path = Path.of(filename);
        if (Files.notExists(path)) {
            throw new RuntimeException("cannot find " + filename);
        }
        try (var in = Files.lines(path)) {
            var lst = in
                    //.limit(50)
                    .filter(line -> !line.isBlank())
                    .flatMap(line -> Stream.of(line.split("[^a-zA-Z’]+")))
                    .filter(word -> !word.isBlank())
                    .map(String::toLowerCase)
                    .filter(word -> word.length() > minSize)
                    .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
                    .entrySet().stream()
                    .sorted((a, b) -> Long.compare(b.getValue(), a.getValue()))
                    .limit(maxWords)
                    .collect(Collectors.toList());

            var maxFreq = lst.get(0).getValue();
            var minFreq = lst.get(lst.size() - 1).getValue();
            var minFont = 15.0;
            var maxFont = 150.0;
            var scale = (maxFont - minFont) / (maxFreq - minFreq);
            var prefix = """
                         <html>
                         <head> <title>Word Cloud</title </head>
                         <body>
                           <h1>Word Cloud</h1>
                         """.stripIndent();
            var suffix = """
                         </body>
                         </html>
                         """.stripIndent();
            var html = lst.stream()
                    .map(e -> String.format("<span style='font-size:%dpx;'>%s</span>",
                            (long) (scale * e.getValue()),
                            e.getKey()))
                    .sorted((a,b) -> Math.random() < 0.5 ? -1 : +1)
                    .collect(Collectors.joining(System.lineSeparator(), prefix, suffix));
            Files.writeString(Path.of("./word-cloud.html"), html);
        }
    }

}
