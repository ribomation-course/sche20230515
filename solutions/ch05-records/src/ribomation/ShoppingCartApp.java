package ribomation;
import java.util.List;

public class ShoppingCartApp {
    record Product(String name, double price) { }

    record Item(Product product, int count) {
        double amount() {
            return count * product.price();
        }
    }

    record Cart(List<Item> items) {
        double total() {
            return items.stream().mapToDouble(Item::amount).sum();
        }
    }

    public static void main(String[] args) {
        var cart = new Cart(List.of(
                new Item(new Product("Apple", 1.25), 1),
                new Item(new Product("Banan", 2.75), 3),
                new Item(new Product("Coco Nut", 3.5), 2),
                new Item(new Product("Date Plum", 4.25), 2),
                new Item(new Product("Java Book", 5.25), 2),
                new Item(new Product("C++ Book", 3.25), 2)
        ));
        cart.items().forEach(System.out::println);
        System.out.printf("Total %.2f kr%n", cart.total());
    }
}
