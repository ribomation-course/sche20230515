package ribomation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;

public class ClimateDataApp {
    public static void main(String[] args) {
        var app = new ClimateDataApp();
        app.run("./climate-data.txt.gz");
    }

    record DataPoint(LocalDate date, double temperature) {
        static DataPoint create(String ymd, String temp) {
            var date = LocalDate.parse(ymd, DateTimeFormatter.ofPattern("yyyyMMdd"));
            var fahrenheit = Double.parseDouble(temp);
            return new DataPoint(date, (fahrenheit - 32) * (5.0 / 9.0));
        }
        int year() { return date.getYear(); }
    }

    void run(String filename) {
        var dataFile = Path.of(filename);
        if (Files.notExists(dataFile)) {
            throw new RuntimeException("cannot find " + dataFile);
        }

        var startTime = System.nanoTime();
        try (var is = Files.newInputStream(dataFile)) {
            var reader = new BufferedReader(
                    new InputStreamReader(
                            new GZIPInputStream(is)));
            try (reader) {
                reader.lines().parallel()
                        .filter(line -> !line.startsWith("STN---"))
                        .map(line -> line.split("\\s+"))
                        .map(arr -> DataPoint.create(arr[2], arr[3]))
                        .collect(Collectors.groupingBy(
                                DataPoint::year,
                                Collectors.averagingDouble(DataPoint::temperature)
                        ))
                        .entrySet().stream()
                        .sorted(Comparator.comparingInt(Map.Entry::getKey))
                        .forEach(e -> System.out.printf("%d: %.2fC%n", e.getKey(), e.getValue()));
            }
        } catch (Exception x) {
            System.out.printf("Failed: %s%n", x);
        }
        var endTime = System.nanoTime();
        System.out.printf("Elapsed: %.3f seconds%n", (endTime - startTime) * 1E-9);
    }

}
