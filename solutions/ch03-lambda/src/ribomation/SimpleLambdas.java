package ribomation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

public class SimpleLambdas {
    static void repeat(int count, Consumer<Integer> expr) {
        for (var k = 1; k <= count; ++k) expr.accept(k);
    }

    static <T> List<T> transform(List<T> lst, UnaryOperator<T> f) {
        var result = new ArrayList<T>();
        for (var e : lst) result.add(f.apply(e));
        return result;
    }

    public static void main(String[] args) {
        repeat(5, k -> System.out.printf("%d) %s%n", k, "Java is cool"));

        var in  = List.of(1, 2, 3, 4, 5);
        var out = transform(in, n -> n * n);
        System.out.printf("%s --> %s%n", in, out);

        var instr = List.of("a","bb","ccc");
        var outstr = transform(instr, s -> s.toUpperCase());
        System.out.printf("%s --> %s%n", instr, outstr);
    }
}
