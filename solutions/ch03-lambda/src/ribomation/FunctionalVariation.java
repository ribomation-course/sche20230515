package ribomation;

public class FunctionalVariation {
    interface ToUpper {
        String uc(String s);
    }

    static void invoke(String txt, ToUpper f) {
        System.out.printf("%s --> %s%n", txt, f.uc(txt));
    }

    public static void main(String[] args) {
        invoke("anonymous class", new ToUpper() {
            @Override
            public String uc(String s) {
                return s.toUpperCase();
            }
        });

        invoke("lambda expr with param type", (String s) -> s.toUpperCase());
        invoke("lambda without param type", s -> s.toUpperCase());

        invoke("method reference", String::toUpperCase);
    }
}
