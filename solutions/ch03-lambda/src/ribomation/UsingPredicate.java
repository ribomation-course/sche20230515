package ribomation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class UsingPredicate {
    static List<String> filter(List<String> lst, Predicate<String> p) {
        var result = new ArrayList<String>();
        for (var s : lst) if (p.test(s)) result.add(s);
        return result;
    }

    static void invoke(List<String> lst, Predicate<String> p) {
        System.out.printf("%s --> %s%n", lst, filter(lst, p));
    }

    public static void main(String[] args) {
        var lst = List.of("hej", "", "hopp", "", "hipp");

        invoke(lst, new Predicate<>() {
            @Override
            public boolean test(String s) {
                return !s.isEmpty();
            }
        });

        invoke(lst, s -> !s.isEmpty());

        invoke(lst, Predicate.not(String::isEmpty));
    }
}
