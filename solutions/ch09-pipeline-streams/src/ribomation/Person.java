package ribomation;

//name;gender;age;postCode
//Link Vandrill;Male;27;28214
//Fredi Renison;Female;25;23304
public record Person(String name, boolean female, int age, int postCode) {
    public static Person fromCSV(String csv) {
        var fields = csv.split(";");
        return new Person(fields[0],
                fields[1].equals("Female"),
                Integer.parseInt(fields[2]),
                Integer.parseInt(fields[3]));
    }
}
