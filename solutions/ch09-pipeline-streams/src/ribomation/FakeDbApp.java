package ribomation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FakeDbApp {
    public static void main(String[] args) {
        var app = new FakeDbApp();
        app.run();
    }

    record DbData(String name, int age) {
    }

    void run() {
        var payload = new ArrayList<Map<String, String>>();
        payload.add(mk("Anna", 22));
        payload.add(mk("Berit", 32));
        payload.add(mk("", 42));
        payload.add(mk(null, 52));

        var res = payload.stream()
                .peek(System.out::println)
                //.map(m -> m.getOrDefault("name", "NO NAME"))
//                .map(m -> {
//                    var val = m.get("name");
//                    if (val == null) {
//                        val = "NO NAME";
//                    } else if (val.isBlank()) {
//                        val = "BLANK";
//                    }
//                    m.put("name", val);
//                    return m;
//                })
                .map(this::clean)
                .map(m -> new DbData(m.get("name"), Integer.parseInt(m.get("age"))))
                //.count()
                //.forEach(System.out::println);
                .collect(Collectors.toList());
        System.out.println(res);
    }

    Map<String, String> mk(String name, int age) {
        var m = new HashMap<String, String>();
        m.put("name", name);
        m.put("age", Integer.toString(age));
        return m;
    }

    Map<String, String> clean(Map<String, String> m) {
        m.forEach((key, val) -> {
            if (val == null || val.isBlank()) {
                val = "???";
                m.put(key, val);
            }
        });
        return m;
    }


}
