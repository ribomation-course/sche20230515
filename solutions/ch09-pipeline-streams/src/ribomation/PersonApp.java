package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PersonApp {
    public static void main(String[] args) throws Exception {
        var app = new PersonApp();
        app.run("./data.csv");
    }

    void run(String filename) throws IOException {
        var path = Path.of(filename);
        if (Files.notExists(path)) {
            throw new RuntimeException("cannot find " + filename);
        }
        try (var in = Files.lines(path)) {
            in.skip(1)
                    .peek(System.out::println)
                    //.limit(100)
                    //.map(Person::fromCSV)
                    .map(csv -> csv.split(";"))
                    .map(fields -> new Person(fields[0],
                            fields[1].equals("Female"),
                            Integer.parseInt(fields[2]),
                            Integer.parseInt(fields[3])))
                    .filter(Person::female)
                    .filter(p -> 30 <= p.age())
                    .filter(p -> p.age() <= 40)
                    .filter(p -> p.postCode() < 20_000)
                    //.peek(System.out::println)
                    .forEach(System.out::println);
        }
    }
}
