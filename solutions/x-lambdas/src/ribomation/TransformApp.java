package ribomation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class TransformApp {
    static List<Integer> transform(List<Integer> lst, UnaryOperator<Integer> f) {
        var res = new ArrayList<Integer>();
        for (var e : lst) res.add(f.apply(e));
        return res;
    }

    public static void main(String[] args) {
        var lst = List.of(1, 2, 3, 4, 5);
        var res = transform(lst, n -> n * n);
        System.out.printf("%s --> %s%n", lst, res);
        System.out.printf("%s --> %s%n", lst, transform(lst, new Add42()));
        System.out.printf("%s --> %s%n", lst, transform(lst, new UnaryOperator<Integer>() {
            @Override
            public Integer apply(Integer n) {
                return n*n*n;
            }
        }));
    }

    static class Add42 implements UnaryOperator<Integer> {
        @Override
        public Integer apply(Integer n) {
            return n + 42;
        }
    }
}
