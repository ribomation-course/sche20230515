package ribomation;

public class RepeatApp {
    interface RepeatHandler {
        void handle(int n);
    }
    static void repeat(int count, RepeatHandler expr) {
        for (var k = 1; k <= count; ++k) expr.handle(k);
    }

    public static void main(String[] args) {
        repeat(1, new MyHandler());
        repeat(2, new RepeatHandler() {
            @Override
            public void handle(int n) {
                System.out.printf("[anon RepeatHandler] %d - Java is cool%n", n);
            }
        });
        repeat(3, (int x) -> System.out.printf("[lambda w param-type] %d) Java is cool%n", x));
        repeat(4, x -> System.out.printf("[lambda] %d) Java is cool%n", x));
    }

    static class MyHandler implements RepeatHandler {
        @Override
        public void handle(int n) {
            System.out.printf("[MyHandler] %d - Java is cool%n", n);
        }
    }
}
