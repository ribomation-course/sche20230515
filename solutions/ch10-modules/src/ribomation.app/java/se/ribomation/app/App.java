package se.ribomation.app;

import se.ribomation.numbers.FunctionFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.setupLogger();
        app.run(10);
        app.run(15);
        app.run(20);

        app.set(42);
        app.run(10);
    }

    void run(int arg) {
        List<Long> results = FunctionFactory.instance.functionNames().stream()
                .map(name -> FunctionFactory.instance.get(name))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(func -> func.eval(arg))
                .collect(Collectors.toList());

        logger.info(String.format("%d @ %s --> %s",
                arg, FunctionFactory.instance.functionNames(), results));
    }

    void set(int factor) {
        logger.info(String.format("---set(%d)---", factor));
        FunctionFactory.instance.get("set").orElseThrow().eval(factor);
    }

    void setupLogger() {
        InputStream is = getClass().getResourceAsStream("/logging.properties");
        try (is) {
            LogManager.getLogManager().readConfiguration(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    final Logger logger = Logger.getLogger("app");
}

