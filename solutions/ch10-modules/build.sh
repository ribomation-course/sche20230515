#!/usr/bin/env bash
set -eux

rm -rf ./bld
mkdir -p ./bld/classes/ribomation.{numbers,app} ./bld/jars

javac --class-path $(ls ./libs/*.jar | tr '\n' ':') \
      --module-path ./libs \
      -d ./bld/classes/ribomation.numbers \
      $(find ./src/ribomation.numbers/java -name '*.java')

javac --module-path ./bld/classes \
      -d ./bld/classes/ribomation.app \
      $(find ./src/ribomation.app/java -name '*.java')

cp ./src/ribomation.app/resources/logging.properties ./bld/classes/ribomation.app
	  
jar --create --file ./bld/jars/ribomation.numbers.jar \
    -C ./bld/classes/ribomation.numbers .

jar --create --file ./bld/jars/ribomation.app.jar \
    --main-class se.ribomation.app.App \
    -C ./bld/classes/ribomation.app .

(set +x ; echo '---- BUILT ----------------')
tree ./bld
jar --list --file ./bld/jars/ribomation.numbers.jar
jar --list --file ./bld/jars/ribomation.app.jar

(set +x ; echo '---- EXECUTION ----------------')
java --module-path ./bld/jars --module ribomation.app

