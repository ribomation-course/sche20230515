#!/usr/bin/env bash
set -eux

echo "JAVA: $JAVA_HOME"
rm -rf ./bld/ribomation-app.jre

jlink --module-path "$JAVA_HOME/jmods;./bld/jars" \
      --add-modules ribomation.app \
      --output ./bld/ribomation-app.jre \
      --launcher run=ribomation.app/se.ribomation.app.App

set +x
echo '--------------------'
tree -L 2 ./bld/ribomation-app.jre

echo '--------------------'
./bld/ribomation-app.jre/bin/run

echo '--------------------'
du -hs ./bld/ribomation-app.jre
du -hs $JAVA_HOME
