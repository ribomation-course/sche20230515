package ribomation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class App {
    static void invoke(List<Person> lst, Comparator<Person> cmp) {
        var out = new ArrayList<>(lst);
        out.sort(cmp);
        out.forEach(System.out::println);
    }

    public static void main(String[] args) {
        var lst = List.of(
                new Person("Anna", "Conda", 42),
                new Person("Justin", "Time", 31),
                new Person("Inge", "Vidare", 42),
                new Person("Berit", "Conda", 20),
                new Person("Margot", "Vidare", 31)
        );
        System.out.println("----original----");
        lst.forEach(System.out::println);

        System.out.println("----by lastName, anon class----");
        invoke(lst, new Comparator<>() {
            @Override
            public int compare(Person a, Person b) {
                return a.getLastName().compareTo(b.getLastName());
            }
        });
        System.out.println("----by lastName, lambda----");
        invoke(lst, (Person a, Person b) -> a.getLastName().compareTo(b.getLastName()));

        System.out.println("----by lastName method ref----");
        invoke(lst, Comparator.comparing(Person::getLastName));

        System.out.println("----by lastName, age compact----");
        invoke(lst, Comparator.comparing(Person::getLastName).thenComparing(Person::getAge));

        System.out.println("----by age----");
        invoke(lst, Comparator.comparing(Person::getAge));

        System.out.println("----by age, reversed----");
        invoke(lst, Comparator.comparing(Person::getAge).reversed());

        System.out.println("----by lastName, age reversed----");
        invoke(lst, Comparator.comparing(Person::getLastName)
                              .thenComparing(Person::getAge)
                              .reversed());
    }

}
