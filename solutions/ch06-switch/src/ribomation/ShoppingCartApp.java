package ribomation;

import java.util.List;

public class ShoppingCartApp {
    record Product(String name, double price) {
    }

    record Item(Product product, int count) {
        double amount() {
            return count * product.price();
        }
    }

    record Cart(List<Item> items) {
        double totalAmount() {
            return items.stream().mapToDouble(Item::amount).sum();
        }

        int totalCount() {
            return items.stream().mapToInt(Item::count).sum();
        }

        String classify() {
            return switch (totalCount()) {
                case 0 -> "No items in cart";
                case 1 -> "One item in cart";
                case 2, 3, 4 -> "Few items in cart";
                default -> "Many items in cart";
            };
        }
    }

    public static void main(String[] args) {
        var cart = new Cart(List.of(
                new Item(new Product("Apple", 1.25), 1),
                new Item(new Product("Banan", 2.75), 1),
                new Item(new Product("Coco Nut", 3.5), 1),
                new Item(new Product("Date Plum", 4.25), 1),
                new Item(new Product("Java Book", 5.25), 1),
                new Item(new Product("C++ Book", 3.25), 1)
        ));

        System.out.printf("CART: %s%n", new Cart(List.of()).classify());
        System.out.printf("CART: %s%n", new Cart(List.of(new Item(new Product("Apple", 2), 1))).classify());
        System.out.printf("CART: %s%n", new Cart(List.of(new Item(new Product("Apple", 2), 5))).classify());
        System.out.printf("CART: %s%n", new Cart(List.of(
                new Item(new Product("Apple", 2), 1),
                new Item(new Product("Banan", 3), 1),
                new Item(new Product("Coco Nut", 4), 1)
        )).classify());
        System.out.printf("CART: %s%n", cart.classify());
    }

}
