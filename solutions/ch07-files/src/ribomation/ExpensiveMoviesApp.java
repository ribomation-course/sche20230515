package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ExpensiveMoviesApp {
    public static void main(String[] args) throws IOException {
        var app = new ExpensiveMoviesApp();
        app.run();
    }

    void run() throws IOException {
        var movies = load("./movies.csv");
        listExpensive(movies, 5).forEach(System.out::println);
    }

    List<Movie> load(String csvFile) throws IOException {
        var path = Path.of(csvFile);
        if (Files.notExists(path)) {
            throw new RuntimeException("cannot find CSV file: " + csvFile);
        }
        var lst = new ArrayList<Movie>();
        var lines = Files.readAllLines(path);
        lines.subList(1, lines.size())
                .forEach(csv -> lst.add(Movie.fromCSV(csv)));
        return lst;
    }

    List<Movie> listExpensive(List<Movie> movies, int numItems) {
        movies.sort(Comparator.comparing(Movie::price).reversed());
        return movies.subList(0, numItems);
    }
}
