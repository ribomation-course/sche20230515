package ribomation;

//title,price,city
//Eternal Sunshine of the Spotless Mind,96.11,Shinao
public record Movie(String title, double price, String city) {
    public static Movie fromCSV(String csv) {
        var fields = csv.split(";");
        return new Movie(fields[0], Double.parseDouble(fields[1]), fields[2]);
    }
}
