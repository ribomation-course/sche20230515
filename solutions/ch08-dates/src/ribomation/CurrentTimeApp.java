package ribomation;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class CurrentTimeApp {
    static Map<String, ZoneId> zones = Map.of(
            "Stockholm", ZoneId.of("Europe/Stockholm"),
            "Helsinki", ZoneId.of("Europe/Helsinki"),
            "London", ZoneId.of("Europe/London"),
            "New York", ZoneId.of("America/New_York"),
            "San Francisco", ZoneId.of("America/Los_Angeles"),
            "Beijing", ZoneId.of("Asia/Shanghai")
    );

    public static void main(String[] args) {
        zones.forEach((city, zone) -> {
            System.out.printf("%-13s: %s (%s)%n",
                    city,
                    Instant.now()
                            .atZone(zone)
                            .toLocalTime()
                            .format(DateTimeFormatter.ofPattern("HH:mm")),
                    zone);
        });
    }
}
