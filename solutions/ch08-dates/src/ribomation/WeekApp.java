package ribomation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;

public class WeekApp {
    record Interval(LocalDateTime start, LocalDateTime end) {
    }

    Interval previousWeek(LocalDate today) {
        final var MONDAY = 1;
        final var SUNDAY = 7;
        var start = today.minusWeeks(1)
                .with(ChronoField.DAY_OF_WEEK, MONDAY)
                .atStartOfDay();
        var end = today.minusWeeks(1)
                .with(ChronoField.DAY_OF_WEEK, SUNDAY)
                .atTime(23, 59, 59);
        return new Interval(start, end);
    }

    void run(LocalDate today) {
        System.out.printf("today: %s%n", today);
        System.out.printf("prev week: %s%n", previousWeek(today));
    }

    public static void main(String[] args) {
        var app = new WeekApp();
        app.run(LocalDate.now());
    }
}
