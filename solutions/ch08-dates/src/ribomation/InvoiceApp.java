package ribomation;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class InvoiceApp {
    public static void main(String[] args) {
        var app = new InvoiceApp();
        app.run(LocalDate.of(2023, 5, 4));
        app.run(LocalDate.now());
    }

    void run(LocalDate invoiceDate) {
        System.out.printf("invoice: %s --> %s%n",
                invoiceDate, dueDate(invoiceDate));
    }

    LocalDate dueDate(LocalDate invoiceDate) {
        return dueDate(invoiceDate, 30);
    }

    LocalDate dueDate(LocalDate invoiceDate, int paymentPeriod) {
        var d = invoiceDate.plusDays(paymentPeriod);
        var day = d.getDayOfWeek();
        if (day == DayOfWeek.SATURDAY) {
            d = d.plusDays(2);
        } else if (day == DayOfWeek.SUNDAY) {
            d = d.plusDays(1);
        }
        return d;
    }
}
