#!/usr/bin/env -S java --source 20
public class Factorial {
    public static void main(String[] args) {
        var n = args.length == 0 ? 5 : Integer.parseInt(args[0]);
        System.out.printf("factorial(%d) = %d%n", n, factorial(n));
    }

    static long factorial(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return n * factorial(n - 1);
    }
}
