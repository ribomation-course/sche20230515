# Java Modern, from version 8 and up
### 15-17 May 2023

# Links
* [Installation Instructions](./installation-instructions.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/jvm/java-modern/)
* [Extra Exercises](./extra-exercises.md)


# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed, open a GIT BASH terminal window  and type the commands below to clone this repo. 

    mkdir -p ~/java-course/my-solutions
    cd ~/java-course
    git clone <git https url> gitlab

![GIT HTTPS URL](./git-url.png)

During the course, solutions will be push:ed to this repo and you can get these by a `git pull` operation

    cd ~/java-course/gitlab
    git pull

# Build Solution/Demo Programs
All programs are ordinary Java programs and can be compiled using any appropriate tool.
Several of the demo & solutions programs has a Gradle build file and can therefore be built by

    ./gradlew build         # BASH
    gradlew.bat build       # Windows



[//]: # (# Links to Large Files)

[//]: # (For some of the exercises, you might want to use a large input file. Here are some compressed large text files to use.)

[//]: # (* [English Text, 100MB &#40;_38MB compressed_&#41;]&#40;https://docs.ribomation.se/java/java-8/english.100MB.gz&#41;)

[//]: # (* [English Text, 1024MB &#40;_396MB compressed_&#41;]&#40;https://docs.ribomation.se/java/java-8/english.1024MB.gz&#41;)

[//]: # (* [Climate Data, 2,5GB &#40;_432MB compressed_&#41;]&#40;https://docs.ribomation.se/java/java-8/climate-data.txt.gz&#41;)



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
