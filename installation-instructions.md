# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

## Java JDK
The latest version of Java JDK.

## IDE
A decent IDE, such as any of

* JetBrains IntellJ IDEA (*Community*)
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download

## GIT Client
* https://git-scm.com/downloads

