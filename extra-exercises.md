# Extra Exercises

## Word Cloud

1. Download a large text file
    * You can find it here: http://pizzachili.dcc.uchile.cl/texts/nlang/ 
1. Generate a map of `<word,frequency>`, 
   2. sort the data in _descending frequency_ order 
   3. and keep the 100 first entries
1. Convert each `<word,frequency>` element into an HTML `<span>` tag, 
   where the size of the word is proportional to its frequency
1. Put all span tags into an HTML document and view it in a browser

### Hints
Find the _min/max_ frequency and use it to compute a scaling factor

    scale = (maxFont - minFont) / (maxFreq - minFreq)

Generate  `<span>` tags as

    String.format("<span style='font-size:%dpx;'>%s</span>", 
                  scale * freq, word)

Minimal HTML

    <html><body> tags </body></html>

----
# Climate Data

1. Download a GZIP compressed file of climate data from the course site
   https://filedn.com/l99UYltmP904c2rGXovDDem/climate-data.txt.gz
1. Extract the YEAR and the TEMP columns
1. Convert the TEMP elements from Fahrenheit to Celsius
https://en.wikipedia.org/wiki/Fahrenheit
1. Aggregate the average temperature for each year and print out in year order

### Hints

Load the file using an InputStream chain with a `GZIPInputStream`

    InputStream is = new GZIPInputStream(new FileInputStream(gzFileName))
    new BufferedReader(is).lines().parallel(). . .

Skip all heading lines

    STN--- WBAN   YEARMODA    TEMP…

Split each line using one or more spaces as the delimiter (`\s+`)

Keep only the two columns YEARMODA and TEMP

    010460 99999  20160101    37.7 24    25.7 24 ...

_As a first stab_; aggregate all temperatures into one average for all years
